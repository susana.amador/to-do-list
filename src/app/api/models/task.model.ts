export class Task {
    id: Number;
    description: string;
    status: number;
}