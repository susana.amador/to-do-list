import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'; 
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
 
 
const URL_API = 'http://192.168.42.57:8080/api'; 

const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService { 
 
  
  constructor(public http: HttpClient) { }
 
  public extractData(res: Response) {
    let body = res;
    return body || { };
  }

  
  public GetList(Request): Observable<any> {
    return this.http.get(URL_API + Request).pipe(
      map(this.extractData), 
      catchError(this.handleError<any>(Request)) 
    );
  }

  
  public GetById(Request, id): Observable<any> {
    return this.http.get(URL_API + Request + id).pipe(
      map(this.extractData), 
      catchError(this.handleError<any>(Request)) 
    );
  }

  public POST (Request, Parameters): Observable<any> { 
    return this.http.post<any>(URL_API + Request, JSON.stringify(Parameters), httpOptions).pipe(
      map(this.extractData), 
      catchError(this.handleError<any>(Request)) 
    );
  }

  public POST_SinParametros (Request): Observable<any> { 
    return this.http.post<any>(URL_API + Request, httpOptions).pipe(
      map(this.extractData), 
      catchError(this.handleError<any>(Request)) 
    );
  }
  private createRequestOptions() {
      let headers = new HttpHeaders({
          "Content-Type": "application/json"
      });
      return headers;
  }



  public PUT (Request, ID, Parameters): Observable<any> {
    return this.http.put(URL_API + Request + ID, JSON.stringify(Parameters), httpOptions).pipe(
      tap(_ => console.log(`Registro Actualizdo Exitosamente`)),
      catchError(this.handleError<any>(Request))
    );
  }
 
  public Delete (Request, id): Observable<any> {
    return this.http.delete<any>(URL_API + Request + id, httpOptions).pipe(
      tap(_ => console.log(`Registro Eliminado Exitosamente`)),
      catchError(this.handleError<any>(Request))
    );
  } 

  public handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
 
      console.log(`${operation} failed: ${error.message}`);
 
      return of(error.status);
    };
  }
}
