import { Component, ElementRef, ViewChild } from "@angular/core";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
/*-----API------ */

import { User } from "./../api/models/user.model";
import { RestService } from "../api/rest.service";



@Component({
    selector: 'ns-inicio-sesion',
    moduleId: module.id,
    templateUrl: './inicio-sesion.component.html',
    styleUrls: ['./login.component.css'],
    providers: [RestService]
})

export class InicioSesionComponent {

    isLoggingIn = true;
    user: User;
    processing = false;
    @ViewChild("password", { static: false }) password: ElementRef;
    @ViewChild("email", { static: false }) email: ElementRef;

    constructor(private page: Page, private routerExtensions: RouterExtensions, private api: RestService) {
        this.page.actionBarHidden = true;
        this.user = new User();
        this.user.email = "amador@gmail.com";
        this.user.password = "12345678";
    }

    toggleForm() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    submit() {
        if (!this.user.email || !this.user.password) {
            this.alert("Ingrese los datos solicitados");
            return;
        }

        this.processing = true;
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.register()
        }
    }
    data_: any;
    Array_: any = [];
    user_: User;
    login() {
        this.api.POST('/user/auth', { email: this.user.email, password: this.user.password }).subscribe((data: {}) => {
            if (data != 0) {
                this.data_ = data;
                let user_ = {
                    username: this.data_.username,
                    email: this.data_.email,
                    password: null,
                    accessToken: this.data_.accessToken,
                }
                this.data_ = []
                this.data_ = user_;

                let people: User;
                people = this.data_

                if (this.data_.accessToken != "") {
                    this.processing = false;
                    this.routerExtensions.navigate(["/list"], { clearHistory: true });
                }
            } else {
                this.alert('Error al intentar conectar con el servidor, verifique su conexion');
                this.processing = false;

            }

        }, error => { 
            if (error.status == 401) return this.alert('Contraseña Incorrecta'); this.processing = false;
            if (error.status == 404) return this.alert('Verifique su inormación, usuario no encontrado'); this.processing = false;
        });
    }

    register() {
        this.api.POST('/user', { username: this.user.username, email: this.user.email, password: this.user.password }).subscribe((data: {}) => {
            if (data != 0) {
                this.processing = false;
                this.isLoggingIn = true;
                this.alert('Usuario registrado exitosamente')
            } else {
                this.alert('Error al intentar conectar con el servidor, verifique su conexion');
                this.processing = false;

            }
        }, error => {
            this.processing = false;
            this.alert('Error al intentar registrar el usuario, verifique su informacion')
            console.log('error:' + error.message)
        });
    }
    forgotPassword() {
        prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for APP NAME to reset your password.",
            inputType: "email",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result) {
                // this.data_ = this.userService.resetPassword(data.text.trim());
                console.log('Response ForgotPasswod===>>' + this.data_);
            }
        });
    }

    focusPassword() {
        this.password.nativeElement.focus();
    }
    focusEmail() {
        this.email.nativeElement.focus();
    }

    alert(message: string) {
        return alert({
            title: "Información",
            okButtonText: "OK",
            message: message
        });
    }
}