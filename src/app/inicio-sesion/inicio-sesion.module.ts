import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core"; 
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { InicioSesionRoutingModule} from "./inicio-sesion-routing.module";
import { InicioSesionComponent } from "./inicio-sesion.component";  

@NgModule({
    imports: [
        NativeScriptCommonModule,
        InicioSesionRoutingModule,
        NativeScriptHttpClientModule
    ],
    declarations: [ 
        InicioSesionComponent, 
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [ 
    ] 
})
export class InicioSesionModule { }
