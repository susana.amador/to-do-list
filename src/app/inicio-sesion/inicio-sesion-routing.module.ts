import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { InicioSesionComponent } from "./inicio-sesion.component"; 

const routes: Routes = [
    { 
        path: "", 
        component: InicioSesionComponent 
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class InicioSesionRoutingModule { }
