import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { alert, prompt } from "tns-core-modules/ui/dialogs";

/*-----Models--- */
import { Task } from "../api/models/task.model";

/*-----API------ */
import { RestService } from "../api/rest.service";
import { EventData, fromObject } from "tns-core-modules/data/observable";
import { ListView, ItemEventData } from "tns-core-modules/ui/list-view";


/*Modal */
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-list-task',
  moduleId: module.id,
  templateUrl: './list-task.component.html',
  providers: [RestService]
})
export class ListUsersComponent implements OnInit {



  ListTask: any = [];
  token: any;
  responseData: any;

  constructor(private routerExtensions: RouterExtensions, private page: Page, public Api: RestService, public router: Router, private viewContainerRef: ViewContainerRef) {

  }
  ListData: any;
  ngOnInit() {
    this.loadData()

  }

  loadData() {
    this.Api.GetList('/task').subscribe((data: {}) => {
      if (data != 0) {
        this.ListTask = data;
      } else {
        prompt({
          title: "Inormación",
          message: "Error al intentar conectar con el servidor, verifique su conexion",
          okButtonText: "Aceptar"
        })
      }
    }, error => {
      console.log('error:' + error.message + "=======>");
    });
  }

  logout() {
    this.routerExtensions.navigate(["/login"], { clearHistory: true });
  }

  aux: any;
  AddItem() {
    prompt({
      title: "Registrar Nueva tarea",
      message: "Agregue una descripcion",
      inputType: "Descripcion",
      defaultText: "",
      okButtonText: "Ok",
      cancelButtonText: "Cancel"
    }).then((data) => {
      if (data.result) {
        this.PostData(data.text.trim());
      }
    });
  }

  PostData(description_) {
    this.Api.POST('/task', { description: description_, status: null }).subscribe((data: {}) => {
      if (data != 0) {
        this.loadData();
      } else {
        prompt({
          title: "Inormación",
          message: "Error al intentar conectar con el servidor, verifique su conexion",
          okButtonText: "Aceptar"
        })
      }
    }, error => {
      console.log('error:' + error.message + "=======>");
    });
  }

  onListViewLoaded(args: EventData) {
    const listView = <ListView>args.object;
  }

  onItemTap(args: ItemEventData) {
    let task: Task;
    const index = args.index;
    task = this.ListTask[index];

    prompt({
      title: "Editar/Eliminar Registro",
      message: "Si desea eliminar el registro seleccione el boton eliminar, si desea actualizar la informacion, seleccione el boton guardar",
      inputType: "",
      defaultText: "" + task.description,
      okButtonText: "Guardar",
      neutralButtonText: "Eliminar",
      cancelButtonText: "Cancel"
    }).then((data) => {

      if (data.result) {
        this.UpdateData(this.ListTask[index], data.text.trim());
      }

      if (data.result == undefined) {
        this.DeleteData(this.ListTask[index]);
      }
    });

  }

  UpdateData(task, data) {
    this.Api.PUT('/task/', task.id, { description: data }).subscribe((data: {}) => {
      if (data != 0) {
        this.loadData()
      } else {
        prompt({
          title: "Información",
          message: "Error al intentar conectar con el servidor, verifique su conexion",
          okButtonText: "Aceptar"
        })
      }
    }, error => {
      console.log('error:' + error.message + "=======>");
    });
  }

  DeleteData(task) {
    this.Api.Delete('/task/', task.id).subscribe((data: {}) => {
      if (data != 0) {
        this.loadData()
      } else {
        prompt({
          title: "Inormación",
          message: "Error al intentar conectar con el servidor, verifique su conexion",
          okButtonText: "Aceptar"
        })
      }
    }, error => {
      console.log('error:' + error.message + "=======>");
    });
  }

  onTap(args) {
  }
}
