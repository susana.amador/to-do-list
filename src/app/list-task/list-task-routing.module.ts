import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ListUsersComponent } from "./list-task.component"; 

const routes: Routes = [
    { 
        path: "", 
        component: ListUsersComponent 
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ListUsersRoutingModule { }
