import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core"; 
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { registerElement } from "nativescript-angular/element-registry";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { ListUsersRoutingModule} from "./list-task-routing.module";
import { ListUsersComponent } from "./list-task.component";   

registerElement('Fab', () => require('nativescript-floatingactionbutton').Fab);

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ListUsersRoutingModule,
        NativeScriptHttpClientModule
    ],
    declarations: [ 
        ListUsersComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [ 
    ] 
})
export class ListUsersModule { }
